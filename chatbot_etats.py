#!/usr/bin/env python
# -*- coding: utf-8 -*-
# This program is dedicated to the public domain under the CC0 license.

"""
First, a few callback functions are defined. Then, those functions are passed to
the Dispatcher and registered at their respective places.
Then, the bot is started and runs until we press Ctrl-C on the command line.
Usage:
Example of a bot-user conversation using ConversationHandler.
Send /start to initiate the conversation.
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
"""

import logging, sys, requests, time, math

telegram_token = sys.argv[1]

from telegram import (ReplyKeyboardMarkup, ReplyKeyboardRemove)
from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters,
                          ConversationHandler)

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

WHAT_TO_DO, RESTAURANT, DETAIL_RESTAU, END_DETAIL_RESTAU, RECOMMENDATIONS, DETAIL_SORTIE, END_DETAIL_SORTIE = range(7)

TRANSPORT_START, DETAIL_ARRET = range(2)

# fonction appelée au démarrage du bot
# elle permet d'orienter le premier choix de l'utilisateur
def start(update, context):
    reply_keyboard = [['Manger !'], ['Une sortie :)']]

    update.message.reply_text('Salut ! Je suis un super bot qui va t\'aider à trouver un bon restaurant ou une bonne adresse pour sortir :)')
    update.message.reply_text('Tu peux aussi m\'envoyer un /transport à tout moment pour trouver les horaires des transports publics en Suisse 🚂 🚎')
    update.message.reply_text(
        'Que veux-tu faire ?', 
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return WHAT_TO_DO
 
# affiche les styles de restaurants
def choixStyle(update, context):
    reply_keyboard = [['Italien'], ['Asiatique'], ['Français'], ['Espagnol'], ['Vaudoise'], ['Retourner au menu :)']]

    update.message.reply_text(
        'Quel style de cuisine préfères-tu ?',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return RESTAURANT

# affiche les restaurants disponible
def choixRestau(update, context):
    update.message.reply_text('Ah super ! J\'aime aussi beaucoup ce style de cuisine')
    update.message.reply_text('😋😋😋')

    reply_keyboard = [['L\'Aparté'], ['Fiskebar'], ['Bayview'], ['Le Jardin'], ['Revoir les styles']]

    update.message.reply_text(
        'Voici une liste de restaurants proches. Choisis en un pour afficher des détails à son propos 😜\n',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return DETAIL_RESTAU
    
# affiche les détails d'un restaurants
def showDetail(update, context):
    reply_keyboard = [['Revoir les restau', 'J\'y vais !']]

    update.message.reply_text('Dans un cadre épuré et intimiste de quinze couverts seulement, vivez une expérience mémorable et conviviale. En toute simplicité, le chef Armel Bedouet engage le dialogue à votre table pour vous conseiller les produits qu’il a méticuleusement sélectionnés et sublimés. Fort de son expérience au sein de maisons prestigieuses, il exprime sa passion et son talent. Laissez-vous surprendre, le temps d’un aparté.',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return END_DETAIL_RESTAU

# affiche les détails d'une sortie
def choixSortie(update, context):
    reply_keyboard = [['Musée 📖', 'Bar 🍺', 'Club 🍸', '🍕🍕🍕', 'Accueil']]

    update.message.reply_text('Voici les activités que j\'ai à te proposer :',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return RECOMMENDATIONS

# affiche les recommendations pour les sorties
def showRecommendation(update, context):

    text = update.message.text
    text = text[:-2]
    text = text.lower()
    text += 's'
    update.message.reply_text('J\'aime beaucoup les ' + text + ' aussi')
    update.message.reply_text('Voici le top 3 des ' + text + ' à Genève. Redis-moi ce que tu veux faire 👌')

    if (text == 'musées'):
        reply_keyboard = [['Musée des arts d\'Extrême-Orient'], ['Musée Ariana'], ['Musée Barbier-Mueller'], ['Revoir les choix']]
    elif (text == 'bars'):
        reply_keyboard = [['La réserve'], ['Black Tap'], ['Bistrot 23'], ['Revoir les choix']]
    elif (text == 'clubs'):
        reply_keyboard = [['Vert Bouteille'], ['Marius Café'], ['Don Quijote'], ['Revoir les choix']]
 
    update.message.reply_text(
        'Voici la liste',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))


    return DETAIL_SORTIE

# affiche les détails d'une sortie
def showDetailSortie(update, context):
    reply_keyboard = [['Revoir les options', 'Let\'s go !']]

    update.message.reply_text('Un bistrot, bar à vin où l’on aime vivre et partager le bonheur authentique autour d’un verre entre amis. Le Vert Bouteille se veut être un lieu de rencontre sympathique et agréable avec ses murs aux tons olive et une paroi habillée de bouteilles vides. On retrouve un style  » scandinave « , un peu loft industriel, avec des meubles dépareillés, en bois ou Formica, un magnifique bar en zinc et des tabourets design. L’accent est mis sur le naturel et on y sert des vins dits naturels dont le choix est vaste. On ne se lasse pas des tapas en version planchettes ou ardoises avec des produits du terroir et pour la plupart de qualité suisse. Rien de tel que d’accompagner un verre avec une tartine de pesto de tomate séchée, gelée de coing, chèvre frais, jambon de Serrano ! L’été, proche de la place du Marché, la terrasse de ce bar à vin séduit de nombreux adeptes de l’apéritif estival ! Une ambiance sympatique dans un espace authentique et cosy.',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return END_DETAIL_SORTIE

# affiche la carte avec la point d'un restaurant. (ici il y a une valeur par défaut mais qui est modifiable en paramètre lors de l'appel de la fonction)
def showMap(update, context, location=[46.523233, 6.626058]):
    update.message.reply_location(location[0], location[1])
    finDeConv(update, context)

# message de fin du bot
def finDeConv(update, context):
    update.message.reply_text('Super choix !\nPasse une bonne soirée')

    return ConversationHandler.END


#=========================================================================================================================================================
#=========================================================================================================================================================
#=========================================================================================================================================================
# TRANSPORT
# Utile

def appeler_opendata(path):
   url = "http://transport.opendata.ch/v1/" + path
   reponse = requests.get(url)
   return reponse.json()


def calcul_temps_depart(timestamp):
   seconds = timestamp-time.time()
   minutes = math.floor(seconds/60)
   if minutes < 1:
       return "FAUT COURIR!"
   if minutes > 60:
       return "> {} h.".format(math.floor(minutes/60))
   return "dans {} min.".format(minutes)



# Preparation des messages

def afficher_arrets(update, arrets):
   texte_de_reponse = "Voici les arrets:\n"
   for station in arrets['stations']:
       if station['id'] is not None:
           texte_de_reponse += "\n/a" + station['id'] + " " + station['name']
   update.message.reply_text(texte_de_reponse)


def afficher_departs(update, departs):
   texte_de_reponse = "Voici les prochains departs:\n\n"
   for depart in departs['stationboard']:
       texte_de_reponse += "{} {} dest. {} - {}\n".format(
           depart['category'],
           depart['number'],
           depart['to'],
           calcul_temps_depart(depart['stop']['departureTimestamp'])
       )
   texte_de_reponse += "\nAfficher a nouveau: /a" + departs['station']['id']

   coordinate = departs['station']['coordinate']
   update.message.reply_location(coordinate['x'], coordinate['y'])
   update.message.reply_text(texte_de_reponse)



# fonction de lancement du /transport

def transport(update, context):
    update.message.reply_text('Vous pouvez écrire le nom de l\'endroit où vous vous trouvez, soit en pièce jointe 🚏. À toi de choisir 😃')

    return TRANSPORT_START

def lieu_a_chercher(update, context):
   resultats_opendata = appeler_opendata("locations?query=" + update.message.text)
   afficher_arrets(update, resultats_opendata)

   return DETAIL_ARRET


def coordonnees_a_traiter(update, context):
   location = update.message.location
   resultats_opendata = appeler_opendata("locations?x={}&y={}".format(location.latitude, location.longitude))
   afficher_arrets(update, resultats_opendata)

   return DETAIL_ARRET


def details_arret(update, context):
   logger.info(update)
   arret_id = update.message.text[2:]
   afficher_departs(update, appeler_opendata("stationboard?id=" + arret_id))

   transport(update, context)


#=========================================================================================================================================================
#=========================================================================================================================================================
#=========================================================================================================================================================



def cancel(update, context):
    user = update.message.from_user
    logger.info("User %s canceled the conversation.", user.first_name)
    update.message.reply_text('Bye! J\'espère qu\'on reparler bientôt',
                              reply_markup=ReplyKeyboardRemove())

    return ConversationHandler.END


def error(update, context):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, context.error)


def main():
    # Create the Updater and pass it your bot's token.
    # Make sure to set use_context=True to use the new context based callbacks
    # Post version 12 this will no longer be necessary
    updater = Updater(telegram_token, use_context=True)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # Add conversation handler with the states GENDER, PHOTO, LOCATION and BIO
    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('start', start)],

        states={
            WHAT_TO_DO: [MessageHandler(Filters.regex('^(Une sortie :\))$'), choixSortie), MessageHandler(Filters.regex('^(Manger !)$'), choixStyle)],

            RESTAURANT: [MessageHandler(Filters.regex('^(Italien|Asiatique|Français|Espagnol|Vaudoise)$'), choixRestau), MessageHandler(Filters.regex('^(Retourner au menu :\))$'), start)],

            DETAIL_RESTAU: [MessageHandler(Filters.regex('^(L\'Aparté|Fiskebar|Bayview|Le Jardin|Intensus)$'), showDetail), MessageHandler(Filters.regex('^(Revoir les styles)$'), choixStyle)],

            DETAIL_SORTIE: [MessageHandler(Filters.regex('^(Vert Bouteille|Marius Café|Don Quijote|Musée des arts d\'Extrême-Orient|Musée Ariana|Musée Barbier-Mueller|La réserve|Black Tap|Bistrot 23)$'), showDetailSortie), MessageHandler(Filters.regex('^(Revoir les choix)$'), choixSortie)],

            END_DETAIL_RESTAU: [MessageHandler(Filters.regex('^(Revoir les restau)$'), choixRestau), MessageHandler(Filters.regex('^(J\'y vais !)$'), showMap)],

            END_DETAIL_SORTIE: [MessageHandler(Filters.regex('^(Revoir les options)$'), choixSortie), MessageHandler(Filters.regex('^(Let\'s go !)$'), finDeConv)],
        
            RECOMMENDATIONS: [MessageHandler(Filters.regex('^(Musée 📖|Bar 🍺|Club 🍸)$'), showRecommendation), MessageHandler(Filters.regex('^(🍕🍕🍕)$'), choixStyle), MessageHandler(Filters.regex('^(Accueil)$'), start)],
        },

        fallbacks=[CommandHandler('cancel', cancel)]
    )

    # Add conversation handler with the states with transport bot
    transport_handler = ConversationHandler(
        entry_points=[CommandHandler('transport', transport)],

        states={
            TRANSPORT_START: [MessageHandler(Filters.location, coordonnees_a_traiter), MessageHandler(Filters.text, lieu_a_chercher)],
            DETAIL_ARRET: [MessageHandler(Filters.command, details_arret)],
        },

        fallbacks=[CommandHandler('cancel', cancel)]
    )


    dp.add_handler(conv_handler)
    dp.add_handler(transport_handler)

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()